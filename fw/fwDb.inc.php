<?php
/**
* @package bbkFramework
* @subpackage fwDb
* @desc <b>PHP interface for database access</b><br>
* supports ODBC, MSSQL, Sybase, mySQLi <br>
* especially this classes capsual the db layer , so that using code is "independent"
* @author brk@bbk.de, knm@bbk.de
* @copyright 2003, bbk.de
* @version knm, 1.31, 2004.10.07 , bugfixes and new Debug Dialog
* @version knm, 1.39, 2005.06.16 , empty log message
*/
/**
* @internal
*/

// Definition of constants
// The values for these constanst has been taken from MSSQL:
if (!defined ("SQLTEXT"))     define ("SQLTEXT",35);
if (!defined ("SQLVARCHAR"))  define ("SQLVARCHAR",39);
if (!defined ("SQLCHAR"))     define ("SQLCHAR",47);
if (!defined ("SQLINT1"))     define ("SQLINT1",48);
if (!defined ("SQLINT2"))     define ("SQLINT2",52);
if (!defined ("SQLINT4"))     define ("SQLINT4",56);
if (!defined ("SQLBIT"))      define ("SQLBIT",50);
if (!defined ("SQLFLT8"))     define ("SQLFLT8",62);

require_once("fwDef.inc.php");

/**
* fwDbodbc_fetch_array
* @return void
* @desc Because the function odbc_fetch_array doesn't work in PHP 4.3.1, we roll our own , since PHP 4.3.2 the function now works. For compatibility with 4.3.1 we still use our own function
*/
function fwDbodbc_fetch_array($result, $rownumber=-1) {
  
  $rs_assoc = array();
  
  if ($rownumber < 0) {
    odbc_fetch_into($result, $rs);
  }
  else {
    odbc_fetch_into($result, $rs, $rownumber);
  }
  if (is_array($rs)){
    foreach ($rs as $key => $value) {
      $rs_assoc[odbc_field_name($result, $key+1)] = $value;
    }
  }
  return $rs_assoc;
  
  /* since PHP 4.3.2 the function now works. For compatibility with 4.3.1 we still use our own function
  return odbc_fetch_array($result, $rownumber);
  */
}

/**
* fwDbNullConv
* @return string
* @param mixed $pInp Input variable, containing value to be converted
* @param string $pQuotas optional quoatas
* @desc converts a PHP variable value into a SQL String Value including NULL Values <br> if quotas given they are wrapped  
*/
function fwDbNullConv ($pInp, $pQuotas=''){
  if (is_null($pInp)) {return ' NULL ';} else {if((strtolower($pInp))=='null'||($pInp=='')){return ' NULL ';}else{return $pQuotas.$pInp.$pQuotas.' ';}}
}

/**
* fwDbNullEquals
* @return string
* @param mixed $pInp Input variable, containing value to be converted
* @param string $pQuotas optional quoatas
* @desc converts a PHP variable value into a SQL String Value for "=" comparism, if null then "IS NULL" is returned <br> if quotas given they are wrapped  
*/
function fwDbNullEquals($pInp, $pQuotas=''){
  if (is_null($pInp)) {return ' IS NULL ';} else {if((strtolower($pInp))=='null'||($pInp=='')){return ' IS NULL ';}else{return '='.$pQuotas.$pInp.$pQuotas.' ';}}
}

/**
* fwDbNullNotEquals
* @return string
* @param mixed $pInp Input variable, containing value to be converted
* @param string $pQuotas optional quoatas
* @desc converts a PHP variable value into a SQL String Value for "<>" comparism, if null then "IS NOT NULL" is returned <br> if quotas given they are wrapped  
*/
function fwDbNullNotEquals($pInp, $pQuotas=''){
  if (is_null($pInp)) {return ' IS NOT NULL ';} else {if((strtolower($pInp))=='null'||($pInp=='')){return ' IS NOT NULL ';}else{return '<>'.$pQuotas.$pInp.$pQuotas.' ';}}
}

/* --------------------------------------------------------------------------
   ------------------------ fwDbRecordSetClass ------------------------------
   -------------------------------------------------------------------------- */
/**
* fwDbRecordSetClass
* @package bbkFramework
* @subpackage fwDb
* @author brk@bbk.de, knm@bbk.de
* @desc this class contains all you need to handle Query Resultsets (usualy retured from Querys done with fwDb)
*/
class fwDbRecordSetClass {
  // the recordset class
  var $ParentDb;                    // a reference to the fwDb Object, which created the record set
  var $ResultId;                    // the result id of the query (= handle to the result set)
  var $Fields = array();            // buffer array for the upcased fieldnames of the row from the database, indexed from 0..n
  var $Values = array();            // buffer array for the data of the row from the database, indexed by fieldname
  var $DataTypes = array();         // buffer array for the upcased datatypes of each column, indexed by fieldname
  var $FieldsNotUpcased = array();  // buffer array for the fieldnames but not upcased
  var $eof;
  var $RowCount;                    // number of rows in the record set
  var $SQL;                         // the corresponding SQL statement 
  var $xmlMode;                     // 0(default) = data comes from datatbase, 1 = data comes from a xml Document
  var $xmlDoc;                      // the xml document containing the data
  var $xmlRootNode;                 // the root node of the xml document
  var $xmlCurrNode;                 // the current node in the xml tree (corresponds to the current record in a record set)
  //knm : moved to class fwDb	var $charset;						// brj, 30.08.13, charset of the db connection 
  
  
  
  /**
  * @return fwDbRecordSetClass
  * @param fwDb $pParentDb
  * @param int $pResultId 
  * @param string $pSQL
  * @param string $pSQLuntouched *new* SQL string, which is NOT modified by stripslashes, as pSQL is.
  * @desc Constructor for fwDbRecordSetClass
  */
  function fwDbRecordSetClass (&$pParentDb, $pResultId, $pSQL, $pSQLuntouched = false) {
    $this->ParentDb = &$pParentDb;
    $this->ResultId = $pResultId;
    $this->eof = TRUE;
    $this->RowCount = 0;
    $this->SQL = $pSQL;
    $this->SQLu = ($pSQLuntouched) ? $pSQLuntouched : $pSQL;
    $this->xmlMode = 0;
    //knm : moved to class fwDb $this->charset = 'utf8'; // brj, 30.08.13
    
    
  } // function fwDbRecordSetClass
  
  
  
  /**
  * @return void
  * @desc a call to this function reads all records from the recordset,<br>builds a xml document that contains the data <br>
  * and switches the record set to xml mode.<br>
  * you can access the generated xml document by the property $this->xmlDoc. 
  */
  function fwDbXmlBuffering () {
    // create a new xml document that contains only the tag <table>
    $this->xmlDoc = domxml_open_mem ('<table/>');
    $this->xmlRootNode = $this->xmlDoc->document_element();
    $this->fwDbFirst();
    // loop over all records:
    while (!$this->eof) {
      // new row node:
      $rowNode = $this->xmlDoc->create_element('row');
      $this->xmlRootNode->append_child ($rowNode);
      // loop over all fields of a reocrd
      for ($i = 0; $i < count($this->Values); $i++) {
        // we add an attribute for each field to the row node:
        $rowNode->set_attribute ($this->FieldsNotUpcased[$i], $this->Values[$this->Fields [$i]] );
      }
      $this->fwDbNext();
    }
    // get the number of records an store it in RowCount:
    $childs = $this->xmlRootNode->child_nodes();
    if (is_array($childs)) $this->RowCount = count ($childs); else $this->RowCount = 0;
    // the first records becomes the current record:
    $this->xmlCurrNode = $this->xmlRootNode->first_child();
    // we reset the eof flag:
    if  ($this->xmlCurrNode) $this->eof = FALSE; else $this->eof = TRUE;
    // after all, set the record set to xmlMode:
    $this->xmlMode = 1;
  }
 

  
  
  /**
  * @return int
  * @desc Gets the next row from the result set and stores it internally.<br>
  * If the return value is 0, then there are no more rows in the result set (eof).<br>
  * Otherwise 1 is returned.
  */
  function fwDbNext () {
    
    // xml mode, data comes from a xml document:
    if ($this->xmlMode == 1) {
      if (is_a ($this->xmlCurrNode, 'DomNode') || is_a($this->xmlCurrNode, 'php4DOMNode') || is_a($this->xmlCurrNode, 'php4DOMElement')  ) {
        $this->xmlCurrNode = $this->xmlCurrNode->next_sibling();
        if  ($this->xmlCurrNode) {
          $this->eof = FALSE;
          return 1;
        }
        else {
          $this->eof = TRUE;
          return 0;
        }
      }
    }
    
    // normal mode, data comes from a database:
    else {
      switch ($this->ParentDb->Mode) {
        case "ODBC":
        $RowArray = fwDbodbc_fetch_array ($this->ResultId);
        //$RowArray = odbc_fetch_array ($this->ResultId);
        if ($RowArray) {
          $i = 0;
          foreach ($RowArray as $key => $value) {
            $this->Fields [$i] = strtoupper($key);
            $this->Values [$this->Fields [$i]] = $value;
            $this->FieldsNotUpcased [$i] = $key;
            $i += 1;
          }
          $this->eof = FALSE;
          return 1;
        }
        else {
          $this->eof = TRUE;
          return 0;
        }

        case "MYSQL":
        $RowArray = mysql_fetch_assoc ($this->ResultId);
        if ($RowArray) {
          $i = 0;
          foreach ($RowArray as $key => $value) {
            $this->Fields [$i] = strtoupper($key);
            $this->Values [$this->Fields [$i]] = $value;
            $this->FieldsNotUpcased [$i] = $key;
            $i += 1;
          }
          $this->eof = FALSE;
          return 1;
        }
        else {
          $this->eof = TRUE;
          return 0;
        }

        case "MYSQLI":
        $RowArray = (mysqli_fetch_assoc($this->ResultId));
        if ($RowArray) {
          $i = 0;

          foreach ($RowArray as $key => $value) {
            $this->Fields [$i] = strtoupper($key);
			
			//if (mb_detect_encoding($value, 'UTF-8', true) === true) {$value=utf8_decode($value);}
			
			if ($this->ParentDb->charset=='utf8') 
				$this->Values [$this->Fields [$i]] = utf8_decode($value); 
			else
				$this->Values [$this->Fields [$i]] = ($value);
            $this->FieldsNotUpcased [$i] = $key;
            $i += 1;
          }
          $this->eof = FALSE;
          return 1;
        }
        else {
          $this->eof = TRUE;
		  //knm 08.2013 : mysqli_free_result  ( $this->ResultId );
          return 0;
        }
        
        case "MSSQL":
        $RowArray = mssql_fetch_assoc($this->ResultId);
        if ($RowArray) {
          $i = 0;
          foreach ($RowArray as $key => $value) {
            $this->Fields [$i] = strtoupper($key);
            $this->Values [$this->Fields [$i]] = $value;
            $this->FieldsNotUpcased [$i] = $key;
            $i += 1;
          }
          $this->eof = FALSE;
          return 1;
        }
        else {
          $this->eof = TRUE;
          return 0;
        }
        break;
        
        case "SYBASE":
        $RowArray = sybase_fetch_assoc($this->ResultId);
        if ($RowArray) {
          $i = 0;
          foreach ($RowArray as $key => $value) {
            $this->Fields [$i] = strtoupper($key);
            $this->Values [$this->Fields [$i]] = $value;
            $this->FieldsNotUpcased [$i] = $key;
            $i += 1;
          }
          $this->eof = FALSE;
          return 1;
        }
        else {
          $this->eof = TRUE;
          return 0;
        }
        break;
      } // switch ($this-Mode)
    } // else from if ($this->xmlMode == 1)
  } // function fwDbNext
  
  
  /**
  * @return int
  * @desc Moves to the first row of the result set.<br>
  * If the return value is 0, then there are no more rows in the result set (eof).<br>
  * Otherwise 1 is returned.
  */
  function fwDbFirst () {
    
    // xml mode, data comes from a xml document:
    if ($this->xmlMode == 1) {
      $this->xmlCurrNode = $this->xmlRootNode->first_child();
      if  ($this->xmlCurrNode) {
        $this->eof = FALSE;
        return 1;
      }
      else {
        $this->eof = TRUE;
        return 0;
      }
    }
    
    // normal mode, data comes from a database:
    else {
      
      switch ($this->ParentDb->Mode) {
        case "ODBC": {
          $RowArray = fwDbodbc_fetch_array ($this->ResultId,1);
          //$RowArray = odbc_fetch_array ($this->ResultId,1);
          if ($RowArray) {
            $i = 0;
            foreach ($RowArray as $key => $value) {
              $this->Fields [$i] = strtoupper($key);
              $this->Values [$this->Fields [$i]] = $value;
              $this->FieldsNotUpcased [$i] = $key;
              $i = $i + 1;
            }
            $this->eof = FALSE;
            return 1;
          }
          else {
            $this->eof = TRUE;
            return 0;
          }
        }
        case "MYSQL": {
          mysql_data_seek  ($this->ResultId,0);
          return $this->fwDbNext();
          break;
        }
        case "MYSQLI": {
          mysqli_data_seek  ($this->ResultId,0);
          return $this->fwDbNext();
          break;
        }
        case "MSSQL": {
          mssql_data_seek  ($this->ResultId,0);
          return $this->fwDbNext();
          break;
        }
        case "SYBASE": {
          sybase_data_seek  ($this->ResultId,0);
          return $this->fwDbNext();
          break;
        }
      } // switch ($this-Mode)
    } // else from if ($this->xmlMode == 1)
  } // function fwDbFirst
  
  
  
  /**
  * @return mixed
  * @param mixed $pCol
  * @desc Returns the value of the column given by $pCol<br>
  * $pCol can be an integer (as an index) or a fieldname
  */
  function fwDbValue ($pCol) {
    // Returns the value of the column given by $pCol
    // $pCol can be an integer (as an index) or a fieldname
    // if the data is bufferd in a xml document (xmlMode == 1) the data is fetched from there
    
    if (!empty($pCol) or $pCol === 0) {
      
      if (is_string($pCol)) {
        // column is given as a string with its name:
        if ($this->xmlMode == 1) {
          return $this->xmlCurrNode->get_attribute($pCol); 
        }
        else {
          if (isset($this->Values[strtoupper($pCol)]))
          return ($this->Values[strtoupper($pCol)]);
        }
      }
      
      else {
        // column is given as a number:
        if ($this->xmlMode == 1) {
          $attributes = $this->xmlCurrNode->attributes();
          return $attributes[$pCol]->value;
        }
        else {
          return ($this->Values[$this->Fields [$pCol]]);
        }
      }
      
    }
  } // function fwDbValue
  
  
  
  /**
  * @return string
  * @param mixed $pCol
  * @desc Returns the value of the column given by $pCol<br>
  * $pCol can be an integer (as an index) or a fieldname
  */
  function fwDbValueAsGenericPhrase ($pCol, $pQuotes="") {
    // Returns the value of the column given by $pCol with leading "="
    // $pCol can be an integer (as an index) or a fieldname
    // moreover, if it is null it returns "=NULL"
    $val = $this->fwDbValue($pCol);
    return '='.fwDbNullConv($val, $pQuotes);
  } // function fwDbValue
   
  
  
  /**
  * @return string
  * @param mixed $pCol
  * @desc Returns the value of the column given by $pCol<br>
  * $pCol can be an integer (as an index) or a fieldname
  */
  function fwDbValueAsWherePhrase ($pCol, $pQuotes="") {
    // Returns the value of the column given by $pCol with leading "="
    // $pCol can be an integer (as an index) or a fieldname
    // moreover , if it is null it returns "is null"
    $val = $this->fwDbValue($pCol);
    return ''.fwDbNullEquals($val, $pQuotes);
  } // function fwDbValue

  
  
  /**
  * @return boolean
  * @desc moves the recordset to the next result set from the query
 */
  function fwDbNextResultSet () {
    $retVal = "";  
    switch ($this->ParentDb->Mode) {
      case "ODBC": {
        $retVal = odbc_next_result($this->ResultId);
        if ($retVal) {
          $this->Fields = "";
          $this->Values = "";
          $this->DataTypes = "";
          $this->FieldsNotUpcased = "";
          $this->RowCount = odbc_num_rows ($this->ResultId);
          $this->fwDbNext();
        }
        break;
      }
      
      case "MYSQL": {
        $retVal = mysql_next_result ($this->ResultId);
        if ($retVal) {
          $this->Fields = "";
          $this->Values = "";
          $this->DataTypes = "";
          $this->FieldsNotUpcased = "";
          $this->RowCount = mysql_num_rows ($this->ResultId);
          $this->fwDbNext();
        }
        break;
      }
      case "MYSQLI": {
        $retVal = mysqli_next_result ($this->ResultId);
        if ($retVal) {
          $this->Fields = "";
          $this->Values = "";
          $this->DataTypes = "";
          $this->FieldsNotUpcased = "";
          $this->RowCount = mysqli_num_rows ($this->ResultId);
          $this->fwDbNext();
        }
        break;
      }
      
      case "MSSQL": {
        $retVal = mssql_next_result ($this->ResultId);
        if ($retVal) {
          $this->Fields = "";
          $this->Values = "";
          $this->DataTypes = "";
          $this->FieldsNotUpcased = "";
          $this->RowCount = mssql_num_rows ($this->ResultId);
          $this->fwDbNext();
        }
        break;
      }
      case "SYBASE": {
        break;
      }
    }
    return $retVal;
  } // function fwDbNextResultSet
  
  
  
  
  /**
  * @return boolean
  * @param mixed $pCol
  * @desc  Returns the value of empty for the column given by $pCol.<br>
  * This is the centralized method 4 testing if the field is filled with something<br>
  * other than 0, "" , '' , NULL
  */
  function fwDbIsEmpty($pCol) {
    $tVar = $this->fwDbValue($pCol);
    return empty($tVar);
  } // fwDbIsEmpty
  
  
  /**
  * @return boolean
  * @param mixed $pCol
  * @desc Returns the value of isset for the column given by $pCol.<br>
  * This is the centralized method 4 testing if the field is a component of the result set
  */
  function fwDbIsSet($pCol) {
    // Returns the value of isset for the column given by $pCol
    // this is the centralized method 4 testing if the field is a component of the result set
    return isset($this->Values[strtoupper($pCol)]);
  } // fwDbIsSet
  
  
  /**
  * @return boolean
  * @param mixed $pCol
  * @desc Returns the true, if the given column has a char-like datatype like char, varchar, date, time etc.
  */
  function fwDbIsCharLike($pCol) {
    if (empty($this->DataTypes)) $this->fwDbGetDataTypesArray();
    $pCol = strtoupper($pCol);
    if (!(strpos ($this->DataTypes[$pCol],"CHAR") === false) or
    !(strpos ($this->DataTypes[$pCol],"DATE") === false) or
    !(strpos ($this->DataTypes[$pCol],"TIME") === false)) {
      return true;
    }
    else return false;
  } // fwDbIsCharLike
  
  
  
  
  /**
  * @return array
  * @desc Returns the array "Fields", which contains the fieldnames
  */
  function fwDbGetFieldsArray ($pNotUpcased = false) {
    if ($pNotUpcased) return $this->FieldsNotUpcased;
    else              return $this->Fields;
  } //fwDbGetFieldsArray
  
  
  /**
  * @return array
  * @desc returns the array Values, which contains the values
  */
  function fwDbGetValuesArray () {
    return $this->Values;
  } //fwDbGetValuesArray
  
  
  /**
  * @return array
  * @desc Fills the array DataTypes which contains the datatype for each column and returns it
  */
  function fwDbGetDataTypesArray () {
  
	$MSQLITypes = Array();     
	$MSQLITypes[1]='TINYINT';
	$MSQLITypes[2]='SMALLINT';
	$MSQLITypes[3]='INTEGER';
	$MSQLITypes[4]='FLOAT';
	$MSQLITypes[5]='DOUBLE';
	$MSQLITypes[7]='TIMESTAMP';
	$MSQLITypes[8]='BIGINT';
	$MSQLITypes[9]='MEDIUMINT';
	$MSQLITypes[10]='DATE';
	$MSQLITypes[11]='TIME';
	$MSQLITypes[12]='DATETIME';
	$MSQLITypes[13]='YEAR';
	$MSQLITypes[16]='IT';
	$MSQLITypes[246]='DECIMAL';
	$MSQLITypes[252]='LONGTEXT';
	$MSQLITypes[253]='VARCHAR';
	$MSQLITypes[254]='CHAR';
    
    switch ($this->ParentDb->Mode) {
      case "ODBC"  : $i = 1; break;
      case "MYSQL" : $i = 0; break;
      case "MYSQLI": $i = 0; break;
      case "MSSQL" : $i = 0; break;
      case "SYBASE": $i = 0; break;
    }
    
    foreach ($this->Fields as $value) {
      switch ($this->ParentDb->Mode) {
        case "ODBC":
        $this->DataTypes [$value] = strtoupper(odbc_field_type ($this->ResultId,$i));
        break;
        case "MYSQL":
        $this->DataTypes [$value] = strtoupper(mysql_field_type  ($this->ResultId,$i));
        break;
        case "MYSQLI":
		 $myField = mysqli_fetch_field_direct($this->ResultId,$i);
		 if ($myField)
		 $this->DataTypes [$value] = $MSQLITypes[$myField->type];
		 else 
		 $this->DataTypes [$value] = '';
        break;
        case "MSSQL":
        $this->DataTypes [$value] = strtoupper(mssql_field_type  ($this->ResultId,$i));
        break;
        case "SYBASE":
        $this->DataTypes [$value] = "function missing in Sybase functions";
        break;
      }
      
      $i = $i + 1;
    }
	
    return $this->DataTypes;
  } //fwDbGetDataTypesArray
  
  
  /**
  * @return void
  * @desc Dumps the content of the record set in a table
  */
  function fwDbToolDumpDataHTML () {
    print "<table border = 1><tr>";
    foreach ($this->FieldsNotUpcased as $Fieldname)
    print "<td><b>" . $Fieldname. "</b></td>";
    print "</tr>";
    //$this->fwDbFirst();
    while (!$this->eof) {
      print "<tr>";
      foreach ($this->Values as $Value)
      print "<td>" . $Value. "</td>";
      print "</tr>";
      $this->fwDbNext();
    }
    print "</table>";
  }

  
   /**
  * @return void
  * @desc returns a HTML String that contain a dump of the content of the record set in a table
  */
  function fwDbToolGenDumpDataHTML () {
    $retStr = "<table border = 1><tr>";
    foreach ($this->FieldsNotUpcased as $Fieldname)
    $retStr .= "<td><b>" . $Fieldname. "</b></td>";
    $retStr .= "</tr>";
    //$this->fwDbFirst();
    while (!$this->eof) {
      $retStr .= "<tr>";
      foreach ($this->Values as $Value)
      $retStr .= "<td>" . $Value. "</td>";
      $retStr .= "</tr>";
      $this->fwDbNext();
    }
    $retStr .= "</table>";
    return $retStr;
  }
 
  
  
  
  /**
  * @return int
  * @desc Frees the result memory
  */
  function fwDbFreeResult () {
    // brk, 20.1.04: this function is not needed and costs only time
    //return (true);
    
    if ($this->xmlMode == 1) {
    }
    else {
      if ($this->ResultId) {
        switch ($this->ParentDb->Mode) {
          case "ODBC":
          return odbc_free_result ($this->ResultId);
          case "MYSQL":
          return mysql_free_result ($this->ResultId);
          case "MYSQLI":
          return mysqli_free_result ($this->ResultId);
          case "MSSQL":
          return mssql_free_result ($this->ResultId);
          case "SYBASE":
          return sybase_free_result ($this->ResultId);
        } // switch ($this->Mode)
      }
    }
  } // function fwDbFreeResult
  
  
} //class fwDbRecordSetClass



/* --------------------------------------------------------------------------
   -------------------------- fwDbParamClass --------------------------------
   -------------------------------------------------------------------------- */
/**
* fwDbParamClass
* @package bbkFramework
* @subpackage fwDb
* @author brk@bbk.de, knm@bbk.de
* @desc (experimantal) this class contains all you need, to handle parameter I/O to Stored Procedures 
*/
class fwDbParamClass {
  // This class is used to store the information about one parameter for the call to a stored procedure
  var $ParamName;
  var $VarRef;
  var $Type;
  var $IsOuput;
  var $IsNull;
  var $Maxlen;
} // class fwdbParam


/* --------------------------------------------------------------------------
   --------------------- fwDbStoredProcedureClass ---------------------------
   -------------------------------------------------------------------------- */

/**
* fwDbStoredProcedureClass
* @package bbkFramework
* @subpackage fwDb
* @author brk@bbk.de, knm@bbk.de
* @desc (experimantal) this class contains all you need to handle Stored Procedures 
*/
class fwDbStoredProcedureClass {
  
  var $ParentDb;    // a reference to the fwDb Object, which created the StoredProcedure
  var $SPName;      // the name of the stored procdure
  var $SPId;        // the id of stored procedure (MSSQL only)
  var $ParamsArr;   // Array which stores the parameters for the call to a stored procedure
  
  
  /**
  * @return fwDbStoredProcedureClass
  * @param fwDb $pParentDb 
  * @param string $pSPName 
  * @desc The constructor for the fwDbStoredProcedureClass
  */
  function fwDbStoredProcedureClass (&$pParentDb, $pSPName) {
    $this->ParentDb = &$pParentDb;
    $this->SPName   = $pSPName;
    
    switch ($this->ParentDb->Mode) {
      case "ODBC":
      break;
      case "MYSQL":
      break;
      case "MYSQLI":
      break;
      case "MSSQL":
      $this->SPId = mssql_init ($this->SPName,$this->ParentDb->ConnId);
      break;
      case "SYBASE":
      break;
    } // switch ($this->Mode)
  } // function fwDbStoredProcedureClass
  
  
  /**
  * @return void
  * @param string $pParamName
  * @param mixed $pVar
  * @param int $pType
  * @param boolean $pIsOuput
  * @param boolean $pIsNull 
  * @param int $pMaxlen 
  * @desc Adds a parameter for the call to the stored procedure and binds it to the pVar<br>
  * pParamName   : the name of the parameter in the sp<br>
  * pVar         : variable which has to be bound to the parameter of the sp<br>
  * pType        : SQLTEXT, SQLVARCHAR, SQLCHAR, SQLINT1,SQLINT2, SQLINT4, SQLBIT, SQLFLT8<br>
  * pIsOuput     : set it to TRUE, if the parameter is an output parameter<br>
  * pIsNull      : set it to TRUE, when you want to pass a NULL value<br>
  * pMaxlen      : used with char/varchar, should have the same length a the parameter<br><br>
  * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!<br>
  * Because there seems to be some bugs around the output parameters in stored procedures,<br>
  * we always have to make a spezial select statement when leaving a stored procedures, e.g:<br>
  * SELECT 0 as Status, @a as a, @b as b ...(for each output parameter)<br>
  * The PHP script then stores these values in the output vars.<br>
  * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  */
  function fwDbBindParm ($pParamName, &$pVar, $pType, $pIsOuput = FALSE, $pIsNull = FALSE, $pMaxlen=1024) {
    
    // The params are stored in the ParamsArr
    $p = new fwDbParamClass;
    $p->ParamName = $pParamName;
    $p->VarRef    = &$pVar;
    $p->Type      = $pType;
    $p->IsOuput   = $pIsOuput;
    $p->IsNull    = $pIsNull;
    $p->Maxlen    = $pMaxlen;
    $this->ParamsArr [strtoupper($p->ParamName)] = $p;
    
  } // function fwDbBindParm
  
  
  /**
  * @return mixed
  * @desc Executes the stored procedure, after all params from the ParamsArr are processed.<br>
  * The returned value is the value of the field "Status" from the final SELECT statment<br>
  * The values of the returned output parameters are stored in the corresponding variables.
  */
  function fwDbExecute () {
    $ReturnStatus = "fwDbExecute() : no Status returned from stored procedure ";
    
    switch ($this->ParentDb->Mode) {
      case "ODBC":
      // building the query for the execution of the stored procedure:
      $Exec = "EXEC ".$this->SPName." ";
      foreach ($this->ParamsArr as $P)
      $Exec = $Exec.$P->VarRef.",";
      if ($Exec{strlen($Exec)-1} == ",") $Exec{strlen($Exec)-1} = " ";
      $ResultId = odbc_exec ($this->ParentDb->ConnId,$Exec);
      $ResultArr = fwDbodbc_fetch_array($ResultId);
      //$ResultArr = odbc_fetch_array($ResultId);
      
      // Store the returned values for the output parameter in the associated variables:
      foreach ($ResultArr as $Key=>$Val) {
        $KeyUp = strtoupper($Key);
        if ($KeyUp == "STATUS") $ReturnStatus = $Val;
        if (array_key_exists ($KeyUp,$this->ParamsArr)) {
          if ($this->ParamsArr[$KeyUp]->IsOuput) {
            $this->ParamsArr[$KeyUp]->VarRef = $Val;
          }
        }
      }
      return $ReturnStatus;

      case "MYSQL":
      // building the query for the execution of the stored procedure:
      $Exec = "CALL (".$this->SPName." ";
      foreach ($this->ParamsArr as $P)
      $Exec = $Exec.$P->VarRef.",";
      if ($Exec{strlen($Exec)-1} == ",") $Exec{strlen($Exec)-1} = " ";
      $ResultId = mysql_query ($this->ParentDb->ConnId,$Exec);
      $ResultArr = mysql_fetch_assoc($ResultId);
      
      // Store the returned values for the output parameter in the associated variables:
      foreach ($ResultArr as $Key=>$Val) {
        $KeyUp = strtoupper($Key);
        if ($KeyUp == "STATUS") $ReturnStatus = $Val;
        if (array_key_exists ($KeyUp,$this->ParamsArr)) {
          if ($this->ParamsArr[$KeyUp]->IsOuput) {
            $this->ParamsArr[$KeyUp]->VarRef = $Val;
          }
        }
      }
      return $ReturnStatus;

      case "MYSQLI":
		//knm 8.2013 if ($ResultId) mysqli_free_result($ResultId);

      // building the query for the execution of the stored procedure:
      $Exec = "CALL (".$this->SPName." ";
      foreach ($this->ParamsArr as $P)
      $Exec = $Exec.$P->VarRef.",";
      if ($Exec{strlen($Exec)-1} == ",") $Exec{strlen($Exec)-1} = " ";
	  
	  if ($this->ParentDb->charset=='utf8') $ResultId = mysqli_query ($this->ParentDb->ConnId,utf8_encode($Exec)); else
      $ResultId = mysqli_query ($this->ParentDb->ConnId,$Exec);
      
/// knm : toDo es kˆnnen mehre Resultsets vorliegen ! diese m¸ssen eigentlich noch vern¸nftig abgearbeitet werden ....  
while  (mysqli_more_results($this->ConnId)) mysqli_next_result(($this->ConnId));          

      $ResultArr = mysqli_fetch_assoc($ResultId);
      
      // Store the returned values for the output parameter in the associated variables:
      foreach ($ResultArr as $Key=>$Val) {
        $KeyUp = strtoupper($Key);
        if ($KeyUp == "STATUS") $ReturnStatus = $Val;
        if (array_key_exists ($KeyUp,$this->ParamsArr)) {
          if ($this->ParamsArr[$KeyUp]->IsOuput) {
            $this->ParamsArr[$KeyUp]->VarRef = $Val;
          }
        }
      }
      return $ReturnStatus;

      case "MSSQL":
      // Binding all parameters:
      foreach ($this->ParamsArr as $P)
      mssql_bind ($this->SPId, "@".$P->ParamName, $P->VarRef, $P->Type, $P->IsOuput, $P->IsNull, $P->Maxlen);
      $ResultId = mssql_execute ($this->SPId);
      $ResultArr = mssql_fetch_assoc ($ResultId);
      // This function doesn't work : array_change_key_case ($ResultArr,CASE_UPPER );
      // Store the returned values for the output parameter in the associated variables:
      foreach ($ResultArr as $Key=>$Val) {
        $KeyUp = strtoupper($Key);
        if ($KeyUp == "STATUS") $ReturnStatus = $Val;
        if (array_key_exists ($KeyUp,$this->ParamsArr)) {
          if ($this->ParamsArr[$KeyUp]->IsOuput) {
            $this->ParamsArr[$KeyUp]->VarRef = $Val;
          }
        }
      }
      return $ReturnStatus;
      
      case "SYBASE":
      // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      // ERRORs when the stored procedures returns a result set
      
      // Therefore we transparently use ODBC Access to execute stored procedures on Sybase.
      // On the server there must always be a ODBC datasource with the same name as the database
      // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      
      /*
      // Building the query for the execution of the stored procedure:
      $Exec = "EXEC ".$this->SPName." ";
      foreach ($this->ParamsArr as $P)
      $Exec = $Exec.$P->VarRef.",";
      if ($Exec{strlen($Exec)-1} == ",") $Exec{strlen($Exec)-1} = " ";
      $ResultId = sybase_query ($Exec, $this->ParentDb->ConnId);
      $ResultArr = sybase_fetch_assoc($this->ResultId);
      // Store the returned values for the output parameter in the associated variables:
      foreach ($ResultArr as $Key=>$Val) {
      $KeyUp = strtoupper($Key);
      if ($KeyUp == "STATUS") $ReturnStatus = $Val;
      if (array_key_exists ($KeyUp,$this->ParamsArr)) {
      if ($this->ParamsArr[$KeyUp]->IsOuput) {
      $this->ParamsArr[$KeyUp]->VarRef = $Val;
      }
      }
      }
      return $ReturnStatus;
      */
      
      // New temp DB-Object to access Sybase via ODBC:
      $tmpDB = new fwDbClass ("ODBC");
      $tmpDB->fwDBConnect ($this->ParentDb->Dsn, $this->ParentDb->User, $this->ParentDb->Password);
      $tmpSp = $tmpDB->fwDbNewStoredProc ($this->SPName);
      $tmpSp->ParamsArr = $this->ParamsArr;
      $ReturnStatus = $tmpSp->fwDbExecute();
      unset ($tmpSp);
      unset ($tmpDB);
      return $ReturnStatus;
      
    } // switch ($this->Mode)
  } // function fwDbExecute
  
  
} // class fwDbStoredProcedureClass



/* --------------------------------------------------------------------------
   ------------------------------ fwDbClass ---------------------------------
   -------------------------------------------------------------------------- */

/**
* fwDbClass
* @package bbkFramework
* @subpackage fwDb
* @author brk@bbk.de, knm@bbk.de
* @desc main wrapper class for all db layers
*/
class fwDbClass {
  // The database class
  var $Mode;      // database mode: ODBC, MSSQL, SYBASE
  var $Server;    // Name of the server, empty for ODBC
  var $Dsn;       // data source name (for ODBC) or database name
  var $User;
  var $Password;
  var $ConnId;    // connection id
  var $msg;
  
  var $charset =  '';
  
  
  var $SQLproductName = 'MSSQL';
  var $SQLnolock = ' with (NOLOCK) ';
  var $SQLrowlock = ' with (ROWLOCK) ';
  var $SQLisnull = 'ISNULL';
  var $SQLdelimiter = '';
  var $SQLprocCall = 'EXEC ';
  var $SQLprocParmPrefix = '';
  var $SQLprocParmPostfix = '';
  var $SQLemptySelect = ' SELECT NULL where 1 = 0 ';
  var $SQLstmtDelimiter = '';
  var $SQLstmtBlockDelimiter = 'GO';
  var $SQLdateDelimiter = '';
  var $SQLstrDECLAREnewId = 'DECLARE @newId int';
  var $SQLstrCallSeqValForNewId = "EXEC st_SeqValFactoryByNameSilent '~TABLEIDFIELD~', @newId  out"; 
  var $SQLparamOut = ' out';
  var $SQLgetdateFkt = 'getdate()';
  
    
  /**
  * @return fwDbClass
  * @param string $pMode database mode
  * @desc constructor for fwDbClass, pMode = "ODBC" or "MSSQL" or "SYBASE"
  */
  function fwDbClass ($pMode) {
    // The constructor. The parameter $pMode determines the database mode
    
    $this->Mode = strtoupper($pMode);
  } // function fwDbClass
  
  
  /**
  * @return void
  * @desc Opens the connection to the database. Useful in remote scripting
  */
  function fwDbReConnect ($pNotPersistent = 0) {
  	switch ($this->Mode) {
  		case "ODBC":
  			if ($pNotPersistent <> 0)
  			//$this->ConnId = odbc_connect ($this->Dsn, $this->User, $this->Password, SQL_CUR_USE_IF_NEEDED );
  			$this->ConnId = odbc_connect ($this->Dsn, $this->User, $this->Password, SQL_CUR_USE_ODBC );
  			else
  			$this->ConnId = odbc_pconnect ($this->Dsn, $this->User, $this->Password, SQL_CUR_USE_ODBC );
  			break;
  		case "MYSQL":
  			//$this->ConnId = mysql_connect ($this->Server, $this->User, $this->Password);
  			//$this->ConnId = mysql_connect ($this->Server, $this->User, $this->Password, 0,65536);
  			$this->ConnId = mysql_pconnect ($this->Server, $this->User, $this->Password, 65536);
  			mysql_select_db ($this->Dsn);
  			break;
  		case "MYSQLI":
  			$this->ConnId =  mysqli_connect( $this->Server, $this->User, $this->Password ,$this->Dsn);
  			mysqli_select_db($this->ConnId,$this->Dsn);		
			mysqli_set_charset($this->ConnId, $this->charset); //brj, 14.8.13: explicitly set the charset!
  			break;
  		case "MSSQL":
  			$this->ConnId = mssql_connect ($this->Server, $this->User, $this->Password);
  			mssql_select_db ($this->Dsn);
  			break;
  		case "SYBASE":
  			$this->ConnId = sybase_connect ($this->Server, $this->User, $this->Password);
  			sybase_select_db ($this->Dsn);
  			break;
  	} // switch ($this-Mode)
  }


  /** 
  * @return void
  * @param string $pDsn 
  * @param string $pUser 
  * @param string $pPassword 
  * @param string $pServer 
  * @desc Opens the connection to the database
  */
  function fwDbConnect ($pDsn, $pUser, $pPassword, $pServer = "", $pNotPersistent = 0) {
    $this->Server   = $pServer;
    $this->Dsn      = $pDsn;
    $this->User     = $pUser;
    $this->Password = $pPassword;
    $this->ConnId   = 0;
    
    $this->fwDbReConnect ($pNotPersistent);
    
  } // function fwDbConnect
  
  
  /**
  * @return void
  * @desc Closes the connection to the database
  */
  function fwDbClose () {
  	if ($this->ConnId) {
  		switch ($this->Mode) {
  			case "ODBC":
  				//odbc_close ($this->ConnId);
  				break;
  			case "MYSQL":
  				mysql_close ($this->ConnId);
  				break;
  			case "MYSQLI":
  				if($this->ConnId) mysqli_close ($this->ConnId);
  				break;
  			case "MSSQL":
  				mssql_close ($this->ConnId);
  				break;
  			case "SYBASE":
  				sybase_close ($this->ConnId);
  				break;
  		} // switch ($this-Mode)
  	} // if ($this->ConnId)
  } // function fwDbClose


  /**
  * @return int
  * @param string $pSQL 
  * @desc Executes the $pSQL.<br>
  * Returns the ResultId or FALSE, if an error occurs.<br>
  * Use this function, if you do NOT get a result set from the statement.
  */
  function fwDbDoSQL ($pSQL, $pbStripSlashes = true) {
  	if ($this->ConnId) {

  		if($pbStripSlashes)
  		$pSQL = stripslashes($pSQL);

  		if(isset($_SESSION['DEBUG']) && $_SESSION['DEBUG'] > 0){
  			$microTs = microtime();
  			$microTsArr = explode(" ",$microTs);
  			$microTs = $microTsArr[0] + $microTsArr[1];
  			$msg = '<tr><td>/*</td><td>'.str_replace('.',',',$microTs).'</td><td>*/&nbsp;</td><td>'.$pSQL.'</td><td>&nbsp;/*pre*/</td></tr><br>';
  			if (($_SESSION['DEBUG'] & FWDEBUGSQL2LOG) > 0 ) $_SESSION['DEBUG.SQL'] .= $msg;
  			if (($_SESSION['DEBUG'] & FWDEBUGSQL2SCR) > 0 ) echo $msg;
  		}

  		switch ($this->Mode) {
  			case "ODBC":
  				//        return odbc_exec ($this->ConnId, $pSQL);
  				$tmpRs = odbc_exec ($this->ConnId, $pSQL);

  				if(isset($_SESSION['DEBUG']) && $_SESSION['DEBUG'] > 0){
  					$microTs = microtime();
  					$microTsArr = explode(" ",$microTs);
  					$microTs = $microTsArr[0] + $microTsArr[1];
  					$msg = '<tr><td>/*</td><td>'.str_replace('.',',',$microTs).'</td><td>*/&nbsp;</td><td>'.$pSQL.'</td><td>&nbsp;/*post*/</td></tr><br>';
  					if (($_SESSION['DEBUG'] & FWDEBUGSQL2LOG) > 0 ) $_SESSION['DEBUG.SQL'] .= $msg;
  					if (($_SESSION['DEBUG'] & FWDEBUGSQL2SCR) > 0 ) echo $msg;
  				}

  				return $tmpRs;

  			case "MSSQL":
  				return mssql_query ($pSQL, $this->ConnId);

  			case "MYSQLI":

				if ($this->charset=='utf8') $tmpRs=mysqli_query ($this->ConnId, utf8_encode($pSQL)); else
  				$tmpRs=mysqli_query ( $this->ConnId,$pSQL);
  				if(isset($_SESSION['DEBUG']) && $_SESSION['DEBUG'] > 0){
  					$microTs = microtime();
  					$microTsArr = explode(" ",$microTs);
  					$microTs = $microTsArr[0] + $microTsArr[1];
  					$msg = '<tr><td>/*</td><td>'.str_replace('.',',',$microTs).'</td><td>*/&nbsp;</td><td>'.$pSQL.'</td><td>&nbsp;/*post*/</td></tr><br>';
  					if (($_SESSION['DEBUG'] & FWDEBUGSQL2LOG) > 0 ) $_SESSION['DEBUG.SQL'] .= $msg;
  					if (($_SESSION['DEBUG'] & FWDEBUGSQL2SCR) > 0 ) echo $msg;
  				}
/// knm : toDo es kˆnnen mehre Resultsets vorliegen ! diese m¸ssen eigentlich noch vern¸nftig abgearbeitet werden ....  
while  (mysqli_more_results($this->ConnId)) mysqli_next_result(($this->ConnId));          

  				return  $tmpRs;

  			case "MYSQL":
  				$tmpRs=mysql_query ($pSQL,$this->ConnId);
  				if(isset($_SESSION['DEBUG']) && $_SESSION['DEBUG'] > 0){
  					$microTs = microtime();
  					$microTsArr = explode(" ",$microTs);
  					$microTs = $microTsArr[0] + $microTsArr[1];
  					$msg = '<tr><td>/*</td><td>'.str_replace('.',',',$microTs).'</td><td>*/&nbsp;</td><td>'.$pSQL.'</td><td>&nbsp;/*post*/</td></tr><br>';
  					if (($_SESSION['DEBUG'] & FWDEBUGSQL2LOG) > 0 ) $_SESSION['DEBUG.SQL'] .= $msg;
  					if (($_SESSION['DEBUG'] & FWDEBUGSQL2SCR) > 0 ) echo $msg;
  				}

  				return  $tmpRs;

  			case "SYBASE":
  				return sybase_query ($pSQL, $this->ConnId);
  		} // switch ($this-Mode)


  	} // if ($this->ConnId)
  } // function fwDbDoSQL


  /**
  * @return fwDbRecordSetClass
  * @param string $pQuery the query to execute
  * @param boolean $pGetLastResultSet if true the last result set of the query will be returned
  * @desc Executes the $pQuery, creates a new fwDbRecordSet and returns it.<br>
  * Use this function, if you get a result set from the Query.
  */
  function fwDbQuery ($psQuery, $pGetLastResultSet=FALSE) {
  	//print $this->Dsn . '<br>';
  	//print $psQuery . '<br>';
  	if ($this->ConnId) {

  		$pQuery = stripslashes($psQuery);
  		// The result of a totaly empty Query produces errors. So we replace it by a correct statements, that returns nothing:
  		if ($pQuery == "") $pQuery = $this->SQLemptySelect;

  		if(isset($_SESSION['DEBUG']) && $_SESSION['DEBUG'] > 0){
  			$microTs = microtime();
  			$microTsArr = explode(" ",$microTs);
  			$microTs = $microTsArr[0] + $microTsArr[1];
  			$msg = '<tr><td>/*</td><td>'.str_replace('.',',',$microTs).'</td><td>*/&nbsp;</td><td>'.$pQuery.'</td><td>&nbsp;/*pre*/</td></tr><br>';
  			if (($_SESSION['DEBUG'] & FWDEBUGSQL2LOG) > 0 ) $_SESSION['DEBUG.SQL'] .= $msg;
  			if (($_SESSION['DEBUG'] & FWDEBUGSQL2SCR) > 0 ) echo $msg;
  		}

  		switch ($this->Mode) {
  			case "ODBC":

  				$ResultId = odbc_exec ($this->ConnId, $pQuery);
  				if ($ResultId) {

  					// if $pGetLastResultSet is true, we have to find the last result set will be returned
  					if ($pGetLastResultSet) {
  						$countResultSet = 0;
  						do $countResultSet++; while(odbc_next_result($ResultId));
  						$ResultId = odbc_exec ($this->ConnId, $pQuery);
  						for ($i = 1; $i < $countResultSet; $i++) odbc_next_result($ResultId);
  					}

  					$rs = new fwDbRecordSetClass ($this, $ResultId, $pQuery, $psQuery);
  					// storing the record count in the record set for further use
  					$rs->RowCount = odbc_num_rows ($ResultId);
  					$rs->fwDbNext(); // reading the first row

  					if(isset($_SESSION['DEBUG']) && $_SESSION['DEBUG'] > 0){
  						$microTs = microtime();
  						$microTsArr = explode(" ",$microTs);
  						$microTs = $microTsArr[0] + $microTsArr[1];
  						$msg = '<tr><td>/*</td><td>'.str_replace('.',',',$microTs).'</td><td>*/&nbsp;</td><td>'.$pQuery.'</td><td>&nbsp;/*post*/</td></tr><br>';
  						if (($_SESSION['DEBUG'] & FWDEBUGSQL2LOG) > 0 ) $_SESSION['DEBUG.SQL'] .= $msg;
  						if (($_SESSION['DEBUG'] & FWDEBUGSQL2SCR) > 0 ) echo $msg;
  					}

  					return $rs;

  				}
  				else {
  					return FALSE;
  				}

  			case "MYSQL":
  				$ResultId = null;
  				$ResultId = mysql_query ($pQuery, $this->ConnId);
  				if ($ResultId) {

  					// if $pGetLastResultSet is true, we have to find the last result set will be returned
  					if ($pGetLastResultSet) {
  						$countResultSet = 0;
  						do $countResultSet++; while(mysql_next_result($ResultId));
  						$ResultId = mysql_query ($pQuery, $this->ConnId);
  						for ($i = 1; $i < $countResultSet; $i++) mysql_next_result($ResultId);
  					}

  					$rs = new fwDbRecordSetClass ($this, $ResultId, $pQuery);
  					// storing the record count in the record set for further use
  					$rs->RowCount = mysql_num_rows ($ResultId);
  					$rs->fwDbNext(); // reading the first row
  					return $rs;
  				}
  				else {
  					return FALSE;
  				}

  			case "MYSQLI":
  				//if (isset($ResultId)) if ($ResultId) mysqli_free_result($ResultId);
  				$ResultId = null;
  				//$this->fwDbReConnect();

				if ($this->charset=='utf8') $ResultId = mysqli_query ( $this->ConnId, utf8_encode($pQuery),  MYSQLI_STORE_RESULT); else
  				$ResultId = mysqli_query ( $this->ConnId, $pQuery,  MYSQLI_STORE_RESULT);
  				if ($this->ConnId->errno)   error_log('[mySQLi Error] '.($this->ConnId->error));

/// knm : toDo es kˆnnen mehre Resultsets vorliegen ! diese m¸ssen eigentlich noch vern¸nftig abgearbeitet werden ....  
while  (mysqli_more_results($this->ConnId)) mysqli_next_result(($this->ConnId));          

  				if ($ResultId) {
  					// if $pGetLastResultSet is true, we have to find the last result set will be returned
  					if ($pGetLastResultSet) {
  						$countResultSet = 0;
  						do $countResultSet++; while(mysqli_next_result($ResultId)); {
						if ($this->charset=='utf8') $ResultId = mysqli_query ($this->ConnId, utf8_encode($pQuery),  MYSQLI_STORE_RESULT); else
  						$ResultId = mysqli_query ($this->ConnId, $pQuery,  MYSQLI_STORE_RESULT);
						}
  						for ($i = 1; $i < $countResultSet; $i++) mysqli_next_result($ResultId);
  					}
  					$rs = new fwDbRecordSetClass ($this, $ResultId, $pQuery);
  					// storing the record count in the record set for further use
  					$rs->RowCount = mysqli_num_rows ($ResultId);
  					$rs->fwDbNext(); // reading the first row

  					if(isset($_SESSION['DEBUG']) && $_SESSION['DEBUG'] > 0){
  						$microTs = microtime();
  						$microTsArr = explode(" ",$microTs);
  						$microTs = $microTsArr[0] + $microTsArr[1];
  						$msg = '<tr><td>/*</td><td>'.str_replace('.',',',$microTs).'</td><td>*/&nbsp;</td><td>'.$pQuery.'</td><td>&nbsp;/*post*/</td></tr><br>';
  						if (($_SESSION['DEBUG'] & FWDEBUGSQL2LOG) > 0 ) $_SESSION['DEBUG.SQL'] .= $msg;
  						if (($_SESSION['DEBUG'] & FWDEBUGSQL2SCR) > 0 ) echo $msg;
  					}
  					return $rs;
  				}
  				else {
  					return FALSE;
  				}

  			case "MSSQL":
  				$ResultId = null;
  				$ResultId = mssql_query ($pQuery, $this->ConnId);
  				if ($ResultId) {

  					// if $pGetLastResultSet is true, we have to find the last result set will be returned
  					if ($pGetLastResultSet) {
  						$countResultSet = 0;
  						do $countResultSet++; while(mssql_next_result($ResultId));
  						$ResultId = mssql_query ($pQuery, $this->ConnId);
  						for ($i = 1; $i < $countResultSet; $i++) mssql_next_result($ResultId);
  					}

  					$rs = new fwDbRecordSetClass ($this, $ResultId, $pQuery);
  					// storing the record count in the record set for further use
  					$rs->RowCount = mssql_num_rows ($ResultId);
  					$rs->fwDbNext(); // reading the first row
  					return $rs;
  				}
  				else {
  					return FALSE;
  				}

  			case "SYBASE":
  				$ResultId = sybase_query ($pQuery, $this->ConnId);
  				if ($ResultId) {
  					$rs = new fwDbRecordSetClass ($this, $ResultId, $pQuery);
  					// storing the record count in the record set for further use
  					$rs->RowCount = sybase_num_rows ($ResultId);
  					$rs->fwDbNext(); // reading the first row
  					return $rs;
  				}
  				else {
  					return FALSE;
  				}
  		} // switch ($this-Mode)



  	} // if ($this->ConnId)
  } // function fwDbQuery


  function fwDbBufferedQuery ($pQuery) {
  	// Executes the query and stores the data as XML in the session.
  	// For the next execution the data will be taken from there, making no access to the db.

  	// generate the name for session var from the query:
  	$nameSessionVar = 'bufferedQuery~' . sha1 ($pQuery);
  	if (isset($_SESSION[$nameSessionVar])) {
  		//$xmlDoc = domxml_open_mem ($_SESSION[$nameSessionVar]);
  		//$rs = $this->fwDbGetRsByXml ($xmlDoc);
  		$rs = $this->fwDbGetRsByXml (domxml_open_mem ($_SESSION[$nameSessionVar]));
  	}
  	else {
  		// data is not in session, execute the query and store the data as xml in the session:
  		$rs = $this->fwDbQuery ($pQuery);
  		$rs->fwDbXmlBuffering();
  		$_SESSION [$nameSessionVar] = $rs->xmlDoc->dump_mem(false);
  	}
  	return $rs;
  }

  
  /**
  * @return fwDbRecordSetClass object in xml mode
  * @param DomDocument $pXmlDoc : xml document must contain the data in the form <table><row field1=value1 field2=value2 ...></row>...</table>
  * @param DomNode $pXmlCurrNode : optional, the node you wants to become the current node
  * @desc returns a record set in xml node build from the given $pXmlDoc
  */
  function fwDbGetRsByXml ($pXmlDoc, $pXmlCurrNode = null) {
    // generate a new record set:
    $rs = new fwDbRecordSetClass ($this, null, null);
    // fill all necessary properties to let it work in xml mode:
    $rs->xmlDoc = $pXmlDoc;
    $rs->xmlRootNode = $rs->xmlDoc->document_element();
    if ($pXmlCurrNode) {
      // if a current node is given, it becomes the current node :-}
      $rs->xmlCurrNode = $pXmlCurrNode;
      $rs->eof = FALSE;
    }
    else  {
      // if no current node is given, the first child of the root becomes the current node
      $rs->xmlCurrNode = $rs->xmlRootNode->first_child();
      if  ($rs->xmlCurrNode) $rs->eof = FALSE; else $rs->eof = TRUE;
    }
    // get the number of records and store it in RowCount:
    $childs = $rs->xmlRootNode->child_nodes();
    if (is_array($childs)) $rs->RowCount = count ($childs); else $rs->RowCount = 0;
    // after all, set the record set to xmlMode:
    $rs->xmlMode = 1;
    return $rs;
  }
  
  
  
  
  /**
  * @return int
  * @param string $pQuery
  * @desc Returns the number of records of the result of the given query<br>  
  * To do this, the query is analyzed and then a select count (1) is executed.
 */
  function fwDbNumRows ($pQuery) {
    #$pQuery = stripslashes($pQuery);
    $numRows = 0;

    // a possible ORDER part must be removed:
    $posOrder = strpos (strtoupper ($pQuery)," ORDER BY");
    if (!($posOrder === false)) $pQuery = substr ($pQuery, 0, $posOrder);

    // a possible GROUP part must be removed:
    $posGroup = strpos (strtoupper ($pQuery)," GROUP BY");
    if (!($posGroup === false)) $pQuery = substr ($pQuery, 0, $posGroup);
    
    // execute a select count (1):
    if ((strpos (strtoupper ($pQuery),"EXEC ") === false)) {
      $rs = $this->fwDbQuery ("SELECT COUNT(1) Num FROM (" . $pQuery . ") Data" );
      $numRows = $rs->fwDbValue ("Num");
      //echo $numRows;
      $rs->fwDbFreeResult ();
    }

    return $numRows;
  } // fwDbNumRows
  
 
  
  /**
  * @return fwDbStoredProcedureClass
  * @param string $pSPName
  * @desc Creates and returns a new fwDbStoredProcedureClass object.
  */
  function fwDbNewStoredProc ($pSPName) {
    $sp = new fwDbStoredProcedureClass ($this, $pSPName);
    return $sp;
  } // function fwDbNewStoredProc
  
  
  
  
  function fwDbNextId ($pTabId){
    // this function to replace the use of Database "autoIncrements" ....mSeq-tabelle
    // in case of generating new data rows this ensures uniqe Id's
    // test existance ....
    $SQL = "select mSeq_Val from mSeq ".$this->SQLnolock." where mSeq_Name = '".$pTabId."'";
    $rs = $this->fwDbQuery($SQL);
    if ($rs->eof) { // ... no entry for this Id-field , create with defaults ...
    $newId = nextID ("mSeq_Id");
    $SQL = "insert into mSeq (mSeq_Id, mSeq_Name, mSeq_Val, mSeq_Offs) values (".$newId.",'".$pTabId."',1000,1)";
    $this->fwDbdoSQL($SQL);
    }
    // increase value ...
    $SQL = "update mSeq ".$this->SQLrowlock." set mSeq_Val = mSeq_Val + mSeq_Offs where mSeq_Name = '".$pTabId."'";
    if($this->fwDbdoSQL($SQL)){
      // get value ...
      $SQL = "select mSeq_Val from mSeq where mSeq_Name = '".$pTabId."'";
      $rs = $this->fwDbQuery($SQL);
      if(!$rs->eof){
        $newVal = $rs->fwDbValue("mSeq_Val");
        return $newVal;
      }
    }
  }
   
  


  /**
  * @return string
  * @param string $pQuery 
  * @param string $pInsertTable 
  * @param string $pTableIdField 
  * @param string $pGenNewIds 
  * @param string $pWithDelete 
  * @param string $pWithGO 
  * @param string $pWithDeclareNewId 
  * @param string $pFieldList 
  * @param string $pValueList 
  * @desc Generates the Insert-Statments in HTML format that are used<br>
  * to insert the data of the given pQuery in to given table pInsertTable.<br>
  * In $pFieldList you can set a ~ separated list of fieldnames. When the insert-statement is generated,<br>
  * the values for these are taken from the (~ separated) $pValueList instead from the DB.
  */
  function fwDbToolGenInsertStatementsHTML ($pQuery, $pInsertTable, $pTableIdField="", $pGenNewIds=0, $pWithDelete=0, $pWithGO=0, $pWithDeclareNewId=1, $pFieldList='', $pValueList='') {
  	$strReturn = "";

  	// split $pFieldList and $pValueList in two arrays:
  	$arrFieldsToReplace = array();
  	$arrValuesToReplace = array();
  	if ($pFieldList > '') {
	  	$arrFieldsToReplace = split ('~', $pFieldList);
	  	$arrValuesToReplace = split ('~', $pValueList);
	  	for ($i = 0; $i < count($arrFieldsToReplace); $i++) { $arrFieldsToReplace[$i] = strtoupper($arrFieldsToReplace[$i]); };
  	}
  	
  	// new recordset to execute the query
  	$rs = $this->fwDbQuery ($pQuery);

  	// some helpful strings:
  	$strPre  = "INSERT INTO ". $pInsertTable ." ( ";
  	$strMid  = " )<br>VALUES ( ";
  	$strPost = " )" . $this->SQLstmtDelimiter . "<br><br>";

  	// generate the string with the fieldnames:
  	$strFields = "";
  	foreach ($rs->FieldsNotUpcased as $FieldName) {
  		if ($strFields != "") $strFields .= ", ";
  		$strFields .= $FieldName;
  	}

  	// generate the string with the values and putting it all together:
  	while (!$rs->eof) {
  		$strCallSeqValFactory = "";
  		$strDelete = "";
  		$strValues = "";
  		foreach ($rs->Values as $FieldName => $value) {
  			if ($strValues != "") $strValues .= ", ";
				
  			// First we check, if the value should have to be replaced by a value from $pValueList:
  			$idxToReplace = array_search($FieldName, $arrFieldsToReplace);
  			if (!($idxToReplace === false)) {
  				$value = $arrValuesToReplace[$idxToReplace];
  			}
  			else {
  				// no, nothing to replace, we take the normal way and get the value from the DB:
  				if ($value == "") {
  					$value = "NULL";
  				}
  				else {

  					if ($rs->fwDbIsCharLike($FieldName)) {

  						// if we have a date or a datetime, we remove the - from the date:
  						if (!(strpos ($rs->DataTypes[$FieldName],"DATE") === false)) {
  							$value = str_replace ("-",$this->SQLdateDelimiter,$value);
  						}

  						// CrLf have to be replaced with a <br>
  						$value = str_replace ("\r\n","<br>",$value);

  						//strings has to be in  '', therefore, if we find a ' in the content, we have to replace it with a double '':
  						$value = "'" . str_replace ("'","''",$value) . "'";
  					}

  					// do we have to generate a delete statement?
  					if ($pWithDelete == 1) {
  						if (strtoupper($pTableIdField) == $FieldName) {
  							$strDelete = "DELETE FROM " . $pInsertTable . " WHERE " . $pTableIdField  . " = " . $value .  $this->SQLstmtDelimiter . "<br><br>";
  						}
  					}

  					// check if the id of the record should be replaced by a construct, which gets the id from mSeq
  					if ($pGenNewIds == 1) {
  						if (strtoupper($pTableIdField) == $FieldName) {
  							if ($pWithDeclareNewId == 1) $strCallSeqValFactory = $this->SQLstrDECLAREnewId . '<br>';
  							//$strCallSeqValFactory .= "EXEC  st_SeqValFactoryByNameSilent '" . $pTableIdField . "', @newId  out<br>";
  							$strCallSeqValFactory .= str_replace('~TABLEIDFIELD~', $pTableIdField, $this->SQLstrCallSeqValForNewId) . '<br>'; 
  							$value = "@newId";
  						}
  					}

  				}
  			}

  			$strValues .= $value;
  		}

  		$strReturn .= $strDelete . $strCallSeqValFactory . $strPre . $strFields . $strMid . $strValues . $strPost;
  		if ($pWithGO == 1) $strReturn .= $this->SQLstmtBlockDelimiter . '<br><br>';

  		$rs->fwDbNext ();
  	} // while (!$rs->eof)
  	$rs->fwDbFreeResult();
  	return $strReturn;
  }


   
  
  /**
  * @return string
  * @param string $pQuery 
  * @param string $pInsertTable 
  * @param string $pIdFields 
  * @desc Generates the Update-Statments in HTML format that are used<br>
  * to update data of the given pQuery in to given table pInsertTable with ghe given $pIdFields
  * !! $pIdFields can be a list of comma seperated fieldnames !!. Together they should be a unique key to the table.
  */
  function fwDbToolGenUpdateStatementsHTML ($pQuery, $pUpdateTable, $pIdFields) {
    $strReturn = '';
    // make an array of the list of id-fieldnames and remove all leading an trailing blanks:
    $arrIdFields = explode (',',$pIdFields);
    for ($i = 0; $i < count($arrIdFields); $i++) $arrIdFields[$i] = trim($arrIdFields[$i]);
    $arrIdValues = array();
    
    // new recordset to execute the query
    $rs = $this->fwDbQuery ($pQuery);
    
    while (!$rs->eof) {
      $strReturn .= 'UPDATE '. $pUpdateTable .' SET ';
      $strComma = '';
      foreach ($rs->FieldsNotUpcased as $fieldName) {
        $strReturn .= $strComma . ' ' . $fieldName . ' = ';
        
        $value = $rs->fwDbValue($fieldName);
        if ($value == "") {
          $value = "NULL";
        }
        else {
          if ($rs->fwDbIsCharLike($fieldName))
          { 
            // if we have a date or a datetime, we remove the - from the date:
            if (!(strpos ($rs->DataTypes[strtoupper($fieldName)],"DATE") === false)) {
              $value = str_replace ("-","",$value);
            }
            //strings has to be in  '', therefore, if we find a ' in the content, we have to replace it with a double '':
            $value = "'" . str_replace ("'","''",$value) . "'";
          }
        }
        $strReturn .= $value;
        // store the values for the id-fields:
        foreach ($arrIdFields as $idField) {
          if (strtoupper($fieldName) == strtoupper ($idField)) {
            $arrIdValues [$fieldName] = $value;
          }
        }
        
        $strComma = ',';
      } // foreach ($rs->FieldsNotUpcased as $FieldName)

      $strReturn .= '<br>WHERE ';
      $strWhereFields = "";
      foreach ($arrIdValues as $idFieldName => $idFieldValue) {
        if ($strWhereFields != '') $strWhereFields .= ' AND ';
        $strWhereFields .= $idFieldName .' = ' . $idFieldValue;
      }
        
      $strReturn .= $strWhereFields . $this->SQLstmtDelimiter . '<br><br>';  
        
      $rs->fwDbNext ();
    } // while (!$rs->eof)
    
    $rs->fwDbFreeResult();
    return $strReturn;
  }
  
   
  
} // class fwDbClass

?>