<?php
/**
* @package bbkFramework
* @subpackage fwDef
* @desc <b>bbk Framework Definitions</b><br>
* Basic overall Definitions made here !
* @author knm@bbk.de
* @version knm, 1.1 , 2004.10.04 , Debug Const
* @version knm, 1.2 , 2004.11.12 , new Debug Switch FWDEBUGCALCXML
* @copyright 2003-2005, bbk.de
*/
/**
* @internal
*/

if (!defined ("FWDEBUGWARNLOG"))  define ("FWDEBUGWARNLOG",1);
if (!defined ("FWDEBUGSQL2LOG"))  define ("FWDEBUGSQL2LOG",2);
if (!defined ("FWDEBUGSQLDATA"))  define ("FWDEBUGSQLDATA",4);


if (!defined ("FWDEBUGCALCLOG"))  define ("FWDEBUGCALCLOG",32);
if (!defined ("FWDEBUGCALCXML"))  define ("FWDEBUGCALCXML",64);
if (!defined ("FWDEBUGSHOW_RS"))  define ("FWDEBUGSHOW_RS",128);

if (!defined ("FWDEBUGWARNSCR"))  define ("FWDEBUGWARNSCR",1 * 256);
if (!defined ("FWDEBUGSQL2SCR"))  define ("FWDEBUGSQL2SCR",2 * 256);
/* space for new flags 



*/
if (!defined ("FWDEBUGPRINT"))    define ("FWDEBUGPRINT"  ,8 * 256);
?>