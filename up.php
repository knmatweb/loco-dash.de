<?php  
/*******************************************************************************
name      : up.php
function  : uploader for Loco Dash  
date      : knm, 2019.02
*******************************************************************************/
require_once("inc/header.inc.php");
require_once("inc/config.inc.php");
require_once("inc/dbconnect.inc.php");
?>
<html>
<head>
</head>
<body>
<?php	

$parms = array(
	'invoice' => array (  'delim' => ';', 'decimalSep' => ',', 'thousandSep' => '.', 'dateFormat' => 'd.m.Y'
						, "SQLins" => "insert into lsdb_invoice values (null, @id@ @valList@);" 
						, "SQLdel" => "delete from lsdb_invoice where lsdb_hash_Idx=@id@ and lsdb_invoice_branchNo=@3@ and lsdb_invoice_No=@14@ ;" 
						, "dTypes" => "C;C;C;C;C;C;C;F;F;F;F;F;D;C;C"
					   )
	
);


if ($_GET['debug']==1) $debug=1; else $debug=0;


dbConnOpen();

function nextID($pId) {
	GLOBAL $glDB;
	$rs = $glDB->fwDbQuery("select lsdb_mSeq_Val, lsdb_mSeq_Inc from lsdb_mSeq where lsdb_mSeq_Code ='".trim($pId)."'");
  	if (!$rs->eof) { 	
	
		$glDB->fwDbDoSQL("update lsdb_mSeq set lsdb_mSeq_Val = " . ( $rs->fwDbValue('lsdb_mSeq_Val')  + $rs->fwDbValue('lsdb_mSeq_Inc') ) . " where lsdb_mSeq_Code ='".trim($pId."'"));
						
		return $rs->fwDbValue('lsdb_mSeq_Val')  + $rs->fwDbValue('lsdb_mSeq_Inc');			
						
	}			
	return "";
}



function write2db ( $data , $pParms, $pHashId) {
	GLOBAL $glDB;
	GLOBAL $debug;
	
	$delSQL = str_replace('@id@',$pHashId,$pParms['SQLdel']);
	$insSQL = str_replace('@id@',$pHashId,$pParms['SQLins']);
	$valStr = "";
	$typeArr = explode(';',strtoupper($pParms['dTypes']));
	
	
	$i = 0;
	
	foreach ($data as $key => $val) {
		
		$i += 1;
		
		switch ($typeArr[$i-1]) {
			case 'F' :
			case 'I' :
						
				$value = str_replace($pParms['thousandSep'],"", $val);
				$value = str_replace($pParms['decimalSep'],".", $value);
				break;
			case 'D':

				$dateArr = date_parse_from_format($pParms['dateFormat'], $val);
				$dateVal = mktime($dateArr['hour'], $dateArr['minute'], $dateArr['second'], $dateArr['month'], $dateArr['day'], $dateArr['year']);
				$value = "'" . strftime( "%Y-%m-%d %H:%M:%S", $dateVal  ) . "'";
				break;
			
			default: // character if C or not defined 
			   $value = "'" . str_replace("'","''", $val) . "'";
		}		
		
		
		$delSQL = str_replace('@'.$i.'@', $value, $delSQL);
		$valStr .= "," . $value;
	}

	
	$insSQL = str_replace('@valList@',$valStr, $insSQL);


	if ($debug) {	
		print '<br>' . $delSQL;
		print '<br>' . $insSQL;
	}
	
	if ($insSQL>'') $glDB->fwDbDoSQL($delSQL);
	if ($insSQL>'') $glDB->fwDbDoSQL($insSQL);

	
	return true;
}



	


function import2db  ($pType, $pFile, $pHashId) {

	global $parms;
	$cnt1 = 0;
	$cnt2 = 0;	

	if(!file_exists($pFile) || !is_readable($pFile)) return FALSE;
	if (($handle = fopen($pFile, 'r')) !== FALSE) {
		
		$header = NULL;
		while (($row = fgetcsv($handle, 1000, $parms[$pType]['delim'])) !== FALSE) {

			if(!$header)
                $header = $row;
			else {
                $data = array_combine($header, $row);
				$cnt1 += 1;
				if (write2db($data, $parms[$pType], $pHashId)) $cnt2 += 1;
			}	
        }
        fclose($handle);

	}
	
	echo "<br>" . "Es wurden " .$cnt2. " von " .$cnt1.  " Sätze verarbeitet";
	return true;	
}


if (isset($_FILES['uploadFile']) and !$_FILES['uploadFile']['error']) {
	
 // print_r($_FILES);
 // print_r($_POST);
	
	
 /* hash verifizieren */ 
  $rs = $glDB->fwDbQuery("select lsdb_hash_Idx, lsdb_hash_name from lsdb_hash where lsdb_hash_Code ='".trim($_POST['hashCode'])."'");
  if (!$rs->eof) { 
	  //echo $rs->fwDbValue("lsdb_hash_Idx");  
	  echo '<br>Zugang erteilt für ' . $rs->fwDbValue('lsdb_hash_Name') . '<br>';
  } else {echo "kein Zugriff ! (Abbruch)"; die;}


/* ... upload  ... */
  $newId = nextID ("csv_upl");
  $nextName = "upl_" . $newId . "_" .  $_FILES['uploadFile']['name'];
  move_uploaded_file($_FILES['uploadFile']['tmp_name'], "./csv/".$nextName);
  printf("Die Datei %s steht jetzt als " .
  $nextName." zur Verfügung.<br />\n",
  $_FILES['uploadFile']['name']);
  printf("Sie ist %u Bytes groß und vom Typ %s.<br />\n",
  $_FILES['uploadFile']['size'], $_FILES['uploadFile']['type']);
	
	
/* alle ok, dann csv in DB .... */	
  //if ($_FILES['uploadFile']['type'] == 'text/csv') 
	  import2db( trim($_POST['tabType']) ,"./csv/".$nextName, $rs->fwDbValue('lsdb_hash_Idx')) ;	
	
	
} // if $_FILES

else

echo(
'<form
  action="'.$_SERVER['PHP_SELF'].'?debug='.$_GET['debug'].'" 
  method="post"
  enctype="multipart/form-data">
<fieldset>
<table><tr>
	<td>CSV Doc upload</td>
	<td><input name="hashCode" value="XAXAXAXA"></td>
	<td><input name="tabType" value="invoice"></td>
	<td><input type="file" accept="application/csv" name="uploadFile" value="" size="30" style="" onChange="onChangeFile(this);"></td>
	<td><input type="submit" value="hochladen!" /></td>
</tr></table></fieldset>
</form>'
);



dbConnClose();
?>
</body>